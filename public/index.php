<?php

declare(strict_types=1);

use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;
use Zend\Diactoros\Response;
use Nyholm\Psr7\Factory\Psr17Factory;

ini_set('display_errors', 'stderr');


require_once __DIR__ . '/../vendor/autoload.php';

/**
 * @var $app Laravel\Lumen\Application
 */

$app = require __DIR__ . '/../bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request
| through the kernel, and send the associated response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
*/


$relay = new Spiral\Goridge\StreamRelay(STDIN, STDOUT);
$psr7 = new Spiral\RoadRunner\PSR7Client(new Spiral\RoadRunner\Worker($relay));
$psr7factory = new Psr17Factory();
$psrHttpFactory = new PsrHttpFactory($psr7factory, $psr7factory, $psr7factory, $psr7factory);
$httpFoundationFactory = new HttpFoundationFactory();

while ($request = $psr7->acceptRequest()) {
    try {
        $httpRequest = $httpFoundationFactory->createRequest($request);
        $httpResponse = $app->dispatch($httpRequest);
        $psr7response = $psrHttpFactory->createResponse($httpResponse);
        $zendResponse = new Response($psr7response->getBody(),$psr7response->getStatusCode(),$psr7response->getHeaders());
        $psr7->respond($zendResponse);
    } catch (Throwable $e) {
        echo $psr7->getWorker()->error((string)$e);
    }
}
