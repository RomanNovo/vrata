<?php

namespace App\Exceptions;

use Exception;

/**
 * Class NotImplementedException
 * @package App\Exceptions
 */
class NotImplementedException extends Exception
{

}