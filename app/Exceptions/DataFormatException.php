<?php

namespace App\Exceptions;

use Exception;

/**
 * Class DataFormatException
 * @package App\Exceptions
 */
class DataFormatException extends Exception
{

}